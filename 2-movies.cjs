/*const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}
*/

//Q1. Find all the movies with total earnings more than $500M. 
function totalEarningsMoreThan$500M(data)
{
    let res = Object.keys(data).reduce((acc,movie)=>{
        if(Number(data[movie].totalEarnings.slice(1,-1))>500)
        {
            acc[movie] = data[movie];
        }
        return acc;
    },{});
    return res;
}

//console.log(totalEarningsMoreThan$500M(favouritesMovies));

//Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
function threeOscarNominationsAndTotalEarningsMoreThan$500M(data)
{
    let res = Object.keys(data).reduce((acc,movie)=>{
        if(data[movie].oscarNominations>3 && Number(data[movie].totalEarnings.slice(1,-1))>500)
        {
            acc[movie] = data[movie];
        }
        return acc;
    },{});
    return res;
}

//console.log(threeOscarNominationsAndTotalEarningsMoreThan$500M(favouritesMovies));

//Q.3 Find all movies of the actor "Leonardo Dicaprio".
function actedByLeonardoDicar(data)
{
    let res = Object.keys(data).reduce((acc,movie)=>{
        if(data[movie].actors.includes("Leonardo Dicaprio"))
        {
            acc[movie] = data[movie];
        }
        return acc;
    },{});
    return res;
}

//console.log(actedByLeonardoDicar(favouritesMovies));

//Q.4 Sort movies (based on IMDB rating)
//if IMDB ratings are same, compare totalEarning as the secondary metric.
function sortMoviesByIMDBAndTotalEarnings(data)
{
    const sortable = Object.fromEntries(
        Object.entries(data).sort(([,a],[,b]) => {
                if(a.imdbRating>b.imdbRating)
                    return 1;
                else if(a.imdbRating<b.imdbRating)
                    return -1;
                else
                {
                    if(Number(a.totalEarnings.slice(1,-1))>Number(b.totalEarnings.slice(1,-1)))
                        return 1;
                    else if(Number(a.totalEarnings.slice(1,-1))<Number(b.totalEarnings.slice(1,-1)))
                        return -1;
                    else
                        return 0;
                }
            })
    );
    return sortable;
}

//console.log(sortMoviesByIMDBAndTotalEarnings(favouritesMovies));

//Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        //drama > sci-fi > adventure > thriller > crime

function groupMoviesByGenre(data)
{
    let groupingMovies = Object.keys(data).reduce((res,movie)=>{
        if(data[movie].genre.includes('drama'))
        {
            res.drama[movie] = JSON.stringify(data[movie]);
        }
        else if(data[movie].genre.includes('sci-fi'))
        {
            res["sci-fi"][movie] = JSON.stringify(data[movie]);
        }
        else if(data[movie].genre.includes('adventure'))
        {
            res.adventure[movie] = JSON.stringify(data[movie]);
        }
        else if(data[movie].genre.includes('thriller'))
        {
            res.thriller[movie] = JSON.stringify(data[movie]);
        }
        else if(data[movie].genre.includes('crime'))
        {
            res.crime[movie] = JSON.stringify(data[movie]);
        }
        return res;
    },{"drama":{}, "sci-fi":{}, "adventure":{}, "thriller":{}, "crime":{}});

    return groupingMovies;
}

//console.log(groupMoviesByGenre(favouritesMovies));